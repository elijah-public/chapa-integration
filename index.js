const express = require('express');
const axios = require('axios');

const app = express();

app.use(express.json());
const PORT = 23001;
app.set('views', './views');
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));

app.listen(PORT, () => {
    console.log(`App listening at http://localhost:${PORT}`)
})

app.get('/', (req, res) => {
    res.render('index');
})

app.post('/pay',async function(req, res) {
    console.log("req.body", req.body);
    const { amount, currency, description, email,first_name,last_name } = req.body;
    try{
        const response = await axios.post('https://api.chapa.co/v1/transaction/initialize', {
            amount: amount,
            currency: currency,
            description: description,
            email: email,
            first_name: first_name,
            last_name: last_name,
            tx_ref: ''+Math.random(),
            redirect_url: 'http://localhost:23001/success',
            cancel_url: 'http://localhost:23001/cancel'
        },{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer CHASECK_TEST-OFkpoL4nCdL2iW0juBMvvqqDGqIk1Vm5'
            },
        });
        if(response.status === 200){
            if(response.data.status === 'success'){
                res.redirect(301,response.data.data.checkout_url);
            }
        }
    }catch(err){
        console.log("err", err.response.data);
    }
})